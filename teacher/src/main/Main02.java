package main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Main02 {

	public static void main(String[] args) {
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
		Connection connection = null;
		try {

			connection = DriverManager.getConnection(
					"jdbc:postgresql:jdbc", "jdbc", "jdbc");

			StringBuffer sb = new StringBuffer();
			sb.append("SELECT");
			sb.append("        *");
			sb.append("    FROM");
			sb.append("        category");
			// sb.append(" WHERE");
			// sb.append(" category_id = 1;");

			PreparedStatement prepareStatement = connection
					.prepareStatement(sb.toString());

			ResultSet rs = prepareStatement.executeQuery();

			while (rs.next()) {
				int c1 = rs.getInt("category_id");
				String c2 = rs.getString("category_name");
				System.out.println(c1);
				System.out.println(c2);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}

	}
}
