package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import dto.Category;
import util.ConnectionManager;

public class CategoryDAO {

	public ArrayList<Category> getAll() {
		ConnectionManager connectionManager = new ConnectionManager();

		try {
			Connection connection = connectionManager
					.getConnection();

			StringBuffer sb = new StringBuffer();
			sb.append("SELECT");
			sb.append("        *");
			sb.append("    FROM");
			sb.append("        category");
			// sb.append(" WHERE");
			// sb.append(" category_id = 1;");

			PreparedStatement prepareStatement = connection
					.prepareStatement(sb.toString());

			ResultSet rs = prepareStatement.executeQuery();

			ArrayList<Category> list = new ArrayList<>();
			while (rs.next()) {
				int c1 = rs.getInt("category_id");
				String c2 = rs.getString("category_name");

				Category category = new Category();
				category.setCategoryId(c1);
				category.setCategoryName(c2);

				list.add(category);
			}
			return list;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			connectionManager.closeConnection();
		}

	}

	public Category getOne(int cid) {
		ConnectionManager connectionManager = new ConnectionManager();

		try {
			Connection connection = connectionManager
					.getConnection();

			StringBuffer sb = new StringBuffer();
			sb.append("SELECT");
			sb.append("        *");
			sb.append("    FROM");
			sb.append("        category");
			sb.append(" WHERE");
			sb.append(" category_id = ?;");

			PreparedStatement prepareStatement = connection
					.prepareStatement(sb.toString());
			prepareStatement.setInt(1, cid);

			ResultSet rs = prepareStatement.executeQuery();
			Category category = null;
			while (rs.next()) {
				int c1 = rs.getInt("category_id");
				String c2 = rs.getString("category_name");

				category = new Category();
				category.setCategoryId(c1);
				category.setCategoryName(c2);

			}
			return category;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			connectionManager.closeConnection();
		}

	}

	public int insert(Category category) {
		ConnectionManager connectionManager = new ConnectionManager();

		try {
			Connection connection = connectionManager
					.getConnection();

			StringBuffer sb = new StringBuffer();
			sb.append("INSERT");
			sb.append("    INTO");
			sb.append("        category(");
			sb.append("            category_id");
			sb.append("            ,category_name");
			sb.append("        )");
			sb.append("    VALUES");
			sb.append("        (");
			sb.append("            ?");
			sb.append("            ,?");
			sb.append("        );");

			PreparedStatement prepareStatement = connection
					.prepareStatement(sb.toString());
			prepareStatement.setInt(1,
					category.getCategoryId());
			prepareStatement.setString(2,
					category.getCategoryName());
			int result = prepareStatement.executeUpdate();
			connectionManager.commitTransaction();
			return result;
		} catch (SQLException e) {
			connectionManager.rollbackTransaction();
			throw new RuntimeException(e);
		} finally {
			connectionManager.closeConnection();
		}

	}

	public int update(Category category) {
		ConnectionManager connectionManager = new ConnectionManager();

		try {
			Connection connection = connectionManager
					.getConnection();

			StringBuffer sb = new StringBuffer();
			sb.append("UPDATE");
			sb.append("        category");
			sb.append("    SET");
			sb.append("        category_name = ?");
			sb.append("    WHERE");
			sb.append("        category_id = ?");

			PreparedStatement prepareStatement = connection
					.prepareStatement(sb.toString());
			prepareStatement.setString(1,
					category.getCategoryName());
			prepareStatement.setInt(2,
					category.getCategoryId());
			int result = prepareStatement.executeUpdate();
			connectionManager.commitTransaction();
			return result;
		} catch (SQLException e) {
			connectionManager.rollbackTransaction();
			throw new RuntimeException(e);
		} finally {
			connectionManager.closeConnection();
		}

	}

	public int delete(int cid) {
		ConnectionManager connectionManager = new ConnectionManager();

		try {
			Connection connection = connectionManager
					.getConnection();

			StringBuffer sb = new StringBuffer();
			sb.append("DELETE");
			sb.append("    FROM");
			sb.append("        category");
			sb.append("    WHERE");
			sb.append("        category_id = ?;");

			PreparedStatement prepareStatement = connection
					.prepareStatement(sb.toString());
			prepareStatement.setInt(1, cid);
			int result = prepareStatement.executeUpdate();
			connectionManager.commitTransaction();
			return result;
		} catch (SQLException e) {
			connectionManager.rollbackTransaction();
			throw new RuntimeException(e);
		} finally {
			connectionManager.closeConnection();
		}

	}
}
