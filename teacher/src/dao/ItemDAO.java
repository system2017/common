package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import dto.Item;
import util.ConnectionManager;

public class ItemDAO {

	public ArrayList<Item> getAll() {
		ConnectionManager connectionManager = new ConnectionManager();

		try {
			Connection connection = connectionManager
					.getConnection();

			StringBuffer sb = new StringBuffer();
			sb.append("SELECT");
			sb.append("        *");
			sb.append("    FROM");
			sb.append("        item");
			// sb.append(" WHERE");
			// sb.append(" item_id = 1;");

			PreparedStatement prepareStatement = connection
					.prepareStatement(sb.toString());

			ResultSet rs = prepareStatement.executeQuery();

			ArrayList<Item> list = new ArrayList<>();
			while (rs.next()) {
				int c1 = rs.getInt("item_id");
				String c2 = rs.getString("item_name");
				int c3 = rs.getInt("price");
				int c4 = rs.getInt("category_id");
				Date c5 = rs.getDate("created_date");

				Item item = new Item();
				item.setItemId(c1);
				item.setItemName(c2);
				item.setPrice(c3);
				item.setCategoryId(c4);
				item.setCreatedDate(c5);
				list.add(item);
			}
			return list;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			connectionManager.closeConnection();
		}

	}

	public Item getOne(int iid) {
		ConnectionManager connectionManager = new ConnectionManager();

		try {
			Connection connection = connectionManager
					.getConnection();

			StringBuffer sb = new StringBuffer();
			sb.append("SELECT");
			sb.append("        *");
			sb.append("    FROM");
			sb.append("        item");
			sb.append(" WHERE");
			sb.append(" item_id = ?;");

			PreparedStatement prepareStatement = connection
					.prepareStatement(sb.toString());
			prepareStatement.setInt(1, iid);

			ResultSet rs = prepareStatement.executeQuery();
			Item item = null;
			while (rs.next()) {
				int c1 = rs.getInt("item_id");
				String c2 = rs.getString("item_name");
				int c3 = rs.getInt("price");
				int c4 = rs.getInt("category_id");
				Date c5 = rs.getDate("created_date");

				item = new Item();
				item.setItemId(c1);
				item.setItemName(c2);
				item.setPrice(c3);
				item.setCategoryId(c4);
				item.setCreatedDate(c5);

			}
			return item;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			connectionManager.closeConnection();
		}

	}

	public int insert(Item item) {
		ConnectionManager connectionManager = new ConnectionManager();

		try {
			Connection connection = connectionManager
					.getConnection();

			StringBuffer sb = new StringBuffer();
			sb.append("INSERT");
			sb.append("    INTO");
			sb.append("        item(");
			sb.append("            item_id");
			sb.append("            ,item_name");
			sb.append("            ,price");
			sb.append("            ,category_id");
			sb.append("            ,created_date");
			sb.append("        )");
			sb.append("    VALUES");
			sb.append("        (");
			sb.append("            ?");
			sb.append("            ,?");
			sb.append("            ,?");
			sb.append("            ,?");
			sb.append("            ,?");
			sb.append("        );");

			PreparedStatement prepareStatement = connection
					.prepareStatement(sb.toString());
			prepareStatement.setInt(1, item.getItemId());
			prepareStatement.setString(2,
					item.getItemName());
			prepareStatement.setInt(3, item.getPrice());
			prepareStatement.setInt(4,
					item.getCategoryId());
			prepareStatement.setDate(5,
					item.getCreatedDate());
			int result = prepareStatement.executeUpdate();
			connectionManager.commitTransaction();
			return result;
		} catch (SQLException e) {
			connectionManager.rollbackTransaction();
			throw new RuntimeException(e);
		} finally {
			connectionManager.closeConnection();
		}

	}

	public int update(Item item) {
		ConnectionManager connectionManager = new ConnectionManager();

		try {
			Connection connection = connectionManager
					.getConnection();

			StringBuffer sb = new StringBuffer();
			sb.append("UPDATE");
			sb.append("        item");
			sb.append("    SET");
			sb.append("        item_name = ?");
			sb.append("        ,price = ?");
			sb.append("        ,category_id = ?");
			sb.append("        ,created_date = ?");
			sb.append("    WHERE");
			sb.append("        item_id = ?");

			PreparedStatement prepareStatement = connection
					.prepareStatement(sb.toString());
			prepareStatement.setString(1,
					item.getItemName());
			prepareStatement.setInt(2, item.getPrice());
			prepareStatement.setInt(3,
					item.getCategoryId());
			prepareStatement.setDate(4,
					item.getCreatedDate());
			prepareStatement.setInt(5, item.getItemId());
			int result = prepareStatement.executeUpdate();
			connectionManager.commitTransaction();
			return result;
		} catch (SQLException e) {
			connectionManager.rollbackTransaction();
			throw new RuntimeException(e);
		} finally {
			connectionManager.closeConnection();
		}

	}

	public int delete(int iid) {
		ConnectionManager connectionManager = new ConnectionManager();

		try {
			Connection connection = connectionManager
					.getConnection();

			StringBuffer sb = new StringBuffer();
			sb.append("DELETE");
			sb.append("    FROM");
			sb.append("        item");
			sb.append("    WHERE");
			sb.append("        item_id = ?;");

			PreparedStatement prepareStatement = connection
					.prepareStatement(sb.toString());
			prepareStatement.setInt(1, iid);
			int result = prepareStatement.executeUpdate();
			connectionManager.commitTransaction();
			return result;
		} catch (SQLException e) {
			connectionManager.rollbackTransaction();
			throw new RuntimeException(e);
		} finally {
			connectionManager.closeConnection();
		}

	}
}
