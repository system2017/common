package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Calc
 */
@WebServlet("/calc")
public class Calc extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			String key1 = request.getParameter("key1");
			String key2 = request.getParameter("key2");
			int k1 = Integer.parseInt(key1);
			int k2 = Integer.parseInt(key2);
			int result = k1 + k2;
			request.setAttribute("re", result);
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/result.jsp");
			requestDispatcher.forward(request, response);
		} catch (NumberFormatException e) {
			response.sendRedirect("/practice/html/send.html");
		}
	}

}
